<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class postController extends Controller
{
    function post($id){
        $posts = DB::table('post')
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->select('users.name', 'posts.thumbnail', 'posts.description','post.tittle')
                ->get();
        return view('main.profile',compact('post'));

    }

    function comment($id){
        $comment = DB::table('comments')
                ->join('post', 'comments.posts_id', '=', 'posts.id')
                ->join('users', 'users.id', '=', 'comments.user_id')
                ->select('post.*', 'comments.body', 'users.name')
                ->get();
        return view('post.profile',compact('comment'));

    }
}
