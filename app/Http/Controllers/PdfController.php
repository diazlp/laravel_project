<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    
    public function test() {
        //shorter code
        $data = "this is a test";
        $pdf = PDF::loadView('pdf.test', compact('data')); //compact from $data
        return $pdf->download('test.pdf');
    }

}
