@extends('form-template')

@section('header-form') Sign In @endsection

@section('content-form')
<div class="login-form">
  <form method="POST" action="{{ route('login') }}">
  @csrf
    <div class="form-group">
      <!-- pengennya ini pake username -->
      <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
    </div>

    <div class="form-group">
      <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>
      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
        @error('password')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>

    <button type="submit" class="btn btn-secondary">
    {{ __('Login') }}
    </button>

    @if (Route::has('password.request'))
      <a class="btn btn-link" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
      </a>
    @endif
                    
    </form>
</div>
@endsection
