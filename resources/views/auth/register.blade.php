@extends('form-template')

@section('header-form') Sign Up @endsection

@section('content-form')
<div class="login-form">
  <form method="POST" action="{{ route('register') }}">
  @csrf

    <!-- nama -->
    <div class="form-group">
      <label for="name" class="col-md-4 col-form-label" >{{ __('Name') }}</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', '') }}" required autocomplete="name" autofocus>
        @error('name')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
        @enderror
            
    </div>
    
    <!-- email -->
    <div class="form-group">
      <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', '') }}" required autocomplete="email">
        @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>

    <!-- username nanti link pake one to one relationship -->
    <div class="form-group">
      <label for="username" class="col-md-4 col-form-label">{{ __('Password') }}</label>
        <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username', '') }}" required autocomplete="username">
          @error('username')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
    </div>

    <!-- password -->
    <div class="form-group">
      <label for="password" class="col-md-4 col-form-label">{{ __('Username') }}</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
          @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
    </div>

    <!-- repeat password -->
    <div class="form-group">
      <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-secondary">
        {{ __('Register') }}
      </button>
    </div>
  </form>
</div>
@endsection
