<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>TaggerBook</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Favicons -->
  <link href="{{ asset('/assets-template/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('/assets-template/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https:-tem/plate//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets-template/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets-template/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets-template/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets-template/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets-template/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets-template/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('/assets-template/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('/assets-template/css/custom-style.css') }}" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v1.5.1
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

  <!-- ======= Header ======= -->
  <header id="header">

  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container ml-3">
        <div class="d-flex justify-content-start align-items-center">
          <h2>@yield('header-form')</h2>
        </div>
      </div>
    </section><!-- End Breadcrumbs -->


    <!-- yield content for section('content') -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

    <!-- Registration & Login Form -->
    <div class="col-md-6 col-sm-12">
      @yield('content-form') 
    </div>



  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Ivan, Diaz, Ubeb</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets-template/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/typed.js/typed.min.js') }}"></script>
  <script src="{{ asset('assets-template/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets-template/js/main.js') }}"></script>
  
  @stack('scripts')

</body>
