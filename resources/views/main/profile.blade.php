@extends('template')

@section('content')
<section id="portfolio" class="portfolio section-bg">
      <div class="container">

        <div class="section-title">
          <h2>hello {{$posts->name}}</h2>
          <p>These all of your post.</p>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="100">

            @foreach ($posts as $item -> $seePost)

                <div class="col-lg-4 col-sm-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                    <img src="{{ asset($seePost->thumbnail) }}" class="img-fluid" alt="">
                    <div class="portfolio-links">
                        <a href="{{ asset('assets-template/img/portfolio/portfolio-1.jpg') }}" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                        <a href="/post/{{$seePost->id}}" title="More Details"><i class="bx bxs-like bx-sm"></i></a>
                    </div>
                    </div>
                </div>
            @endforeach



        </div>

      </div>
    </section><!-- End Portfolio Section -->
@endsection
