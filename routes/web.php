<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test-dompdf', function() {
    //this code is for DOMPDF library
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Hello World</h1>');
    return $pdf->stream();
    //make the controller for PDFController to avoid anonymous function
    //run -> php artisan make:controller PDFController -m (for migration as well)
});

//sweet alert has been implemented successfuly
Route::get('/test-dompdf_1', 'PdfController@test'); //laravel pdf (DOMPDF) has been implemented successfully
Route::get('/text-excel', 'UserController@export'); //laravel excel has been implemented successfully

Route::get('/', function () {
    return view('template');
});

//untuk profile page (dan edit profile)
Route::get('/profile', function () {
    return view('main.profile');
});

//untuk show picture
Route::get('/show', function () {
    return view('main.show');//sementara
});

//untuk login page (nanti dijadikan index)
Route::get('/login-form', function () {
    return view('auth.login');
});

//untuk register form
Route::get('/register-form', function () {
    return view('auth.register');
});

// profile post
Route::get('/post', function () {
    return view('profile');
});

Route::get('/post/{id}', 'postController@home');








//Laravel Auth has been implemented successfully
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
